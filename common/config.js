const fs = require('fs');

const path = process.env.CONFIG_PATH || 'files/config.json';

const getConfig = () => {
  if (!fs.existsSync(path)) fs.writeFileSync(path,
    fs.readFileSync('files/config.template.json', 'utf8')
  );
  return JSON.parse(fs.readFileSync(path, 'utf8'));
};

const setConfig = (config) => {
  fs.writeFileSync(path, JSON.stringify(config, null, 2), 'utf8');
  return config;
};

module.exports = {
  getConfig,
  setConfig
};
