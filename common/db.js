const Datastore = require('nedb'),
  bcrypt = require('bcryptjs');

const db = new Datastore({ filename: process.env.DB_PATH || 'files/home-panel-v2.db', autoload: true });

const getUser = (user, passwordReq = false, cb) => {
  db.findOne({ username: user.username }, (err, doc) => {
    if (err) { cb(err); return; }
    if (!doc) { cb(`No user found for ${user.username}.`); return; }
    if (passwordReq)
      bcrypt.compare(user.password, doc.password, (err, res) => {
        err ? cb(err) :
          !res ? cb('Incorrect password.')
            : cb(null, doc);
      });
    else cb(null, doc);
  });
};

const addUser = (user, cb) => {
  // log.info(`Creating user ${user.username}..`);
  bcrypt.hash(user.password, 10, (err, hash) => {
    err ? cb(err) :
      db.insert({ username: user.username, password: hash },
        (err, newDoc) => err ? cb(err) : cb(null, newDoc)
      );
  });
};

module.exports = {
  getUser,
  addUser
};
