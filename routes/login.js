module.exports = (log, app, jsonParser, db) => {
  app.post('/login/setup', jsonParser, (req, res) => {
    db.getUser({ username: req.body.username }, false, (err, user) => {
      if (!err) {
        log.error(`User ${user.username} found.`); res.status(400).send(`User ${user.username} already exists.`);
        return;
      }
      log.error(err);
      if (!req.body.password) {
        log.error('Bad password'); res.status(400).send('No Password! You must provide one to setup an account!');
      } else {
        db.addUser(req.body, (err) => {
          if (err) { res.status(400).send(err); return; }
          res.status(200).json(require('../common/config').getConfig());
        });
      }
    });
  });
  app.post('/login', jsonParser, (req, res) => {
    db.getUser(req.body, true, (err, user) => {
      if (err) {
        log.error(err); res.status(400).send(err);
        return;
      }
      log.info(`User ${user.username} found.`);
      res.status(200).json(require('../common/config').getConfig(user));
    });
  });
};
