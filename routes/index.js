const login = require('./login'),
  radioRoutes = require('./radio_routes'),
  config = require('./config'),
  db = require('../common/db');

module.exports = (log, app, jsonParser) => {
  app.get('/', (_req, res) => res.render('index', { title: 'Home Panel API' }));
  login(log, app, jsonParser, db);
  config(log, app, jsonParser, db);
  radioRoutes(log, app, jsonParser);
};
