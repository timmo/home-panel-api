const TuneIn = require('node-tunein-radio');

const tunein = new TuneIn();

module.exports = (log, app, jsonParser) => {
  app.post('/radio/search', jsonParser, (req, res) => {
    console.debug('search:', req.body.query);
    tunein.search(req.body.query).then((result) => {
      log.debug(result);
      res.json(result.body);
    }).catch((err) => {
      log.error('error:', err);
      res.status(err.head.status).send(err);
    });
  });
  app.post('/radio/get', jsonParser, (req, res) => {
    tunein.tune_radio(req.body.guide_id).then((result) => {
      log.debug(result);
      var response = req.body;
      response.streams = result.body;
      res.json(response);
    }).catch((err) => {
      log.error('error:', err);
      res.status(err.head.status).send(err);
    });
  });
};
