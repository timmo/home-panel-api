module.exports = (log, app, jsonParser, db) => {
  app.post('/config/set', jsonParser, (req, res) => {
    db.getUser(req.body, true, (err, user) => {
      if (err) {
        log.error(err); res.status(400).send(err);
        return;
      }
      log.info(`User ${user.username} found.`);
      res.status(200).json(require('../common/config').setConfig(req.body.config));
    });
  });
};
