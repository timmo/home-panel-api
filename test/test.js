const request = require('supertest');
const app = require('../index');

describe('App', () => {
  it('has the default page', (done) => {
    request(app)
      .get('/')
      .expect(/Home Panel API is active!/, done);
  });
}); 
