const debug = require('debug')('demo:server'),
  http = require('http'),
  https = require('https'),
  fs = require('fs'),
  createError = require('http-errors'),
  express = require('express'),
  path = require('path'),
  cookieParser = require('cookie-parser'),
  logger = require('morgan'),
  helmet = require('helmet'),
  cors = require('cors'),
  bodyParser = require('body-parser'),
  app = express(),
  SimpleNodeLogger = require('simple-node-logger'),
  opts = {
    timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
  },
  log = SimpleNodeLogger.createSimpleLogger(opts);

// Set log level
log.setLevel(process.env.LOG_LEVEL | 'info');

/**
 * Normalize a port into a number, string, or false.
 */
const normalizePort = (val) => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
};

/**
 * Event listener for HTTP server "error" event.
 */
const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      log.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      log.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
};

/**
 * Event listener for HTTP server "listening" event.
 */
const onListening = () => {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(helmet());
app.use(function (_req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
const jsonParser = bodyParser.json({ type: 'application/json' });

require('./routes')(log, app, jsonParser, {});

// catch 404 and forward to error handler
app.use((_req, _res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

/**
 * Get port from environment and store in Express.
 */
var port = normalizePort(process.env.PORT || '3234');
app.set('port', port);

/**
 * Create HTTP server.
 */
let server;
if (fs.existsSync(process.env.SSL_PATH_CERT || 'fullchain.pem')) {
  server = https.createServer({
    cert: fs.readFileSync(process.env.SSL_PATH_CERT || 'fullchain.pem').toString(),
    key: fs.readFileSync(process.env.SSL_PATH_KEY || 'privkey.pem').toString()
  }, app);
} else {
  server = http.createServer(app);
  log.warn('SSL (HTTPS) is not active!!!');
}

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => log.info(`API is live on port ${port}.`));
server.on('error', onError);
server.on('listening', onListening);

module.exports = app;
