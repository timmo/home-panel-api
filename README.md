# Home Panel API

[![GitHub Release](https://img.shields.io/github/release/timmo001/home-panel-api.svg)](https://github.com/timmo001/home-panel-api/releases)
[![License](https://img.shields.io/github/license/timmo001/home-panel-api.svg)](LICENSE.md)
[![pipeline status](https://gitlab.com/timmo/home-panel-api/badges/master/pipeline.svg)](https://gitlab.com/timmo/home-panel-api/commits/master)
[![Waffle.io - Columns and their card count](https://badge.waffle.io/timmo001/home-panel-api.svg?columns=To%20Do,On%20Hold,In%20Progress,Done)](https://waffle.io/timmo001/home-panel-api)

[![Docker Version][version-shield]][microbadger]
[![Docker Layers][layers-shield]][microbadger]
[![Docker Pulls][pulls-shield]][dockerhub]
[![Anchore Image Overview][anchore-shield]][anchore]

![Supports armhf Architecture][armhf-shield]
![Supports aarch64 Architecture][aarch64-shield]
![Supports amd64 Architecture][amd64-shield]
![Supports i386 Architecture][i386-shield]

[![Buy me a coffee][buymeacoffee-shield]][buymeacoffee]

REST API for [Home Panel](https://github.com/timmo001/home-panel)

## Features

- Searches and gets streams from the TuneIn API.
- SSL Support

## Setup

See docs in [Home Panel](https://github.com/timmo001/home-panel).

## Usage

- GET `/` - Test the api is active

- POST `/radio/search` - Send query in JSON:

  ```json
  {
    "query": "Radio Station"
  }
  ```

- POST `/radio/get` - Send object returned by above request in JSON:

  ```json
  {
    "guide_id": "Radio Station"
  }
  ```

- POST `/login` - Send object returned by above request in JSON:

  ```json
  {
    "username": "username",
    "password": "password"
  }
  ```

- POST `/login/setup` - Send object returned by above request in JSON:

  ```json
  {
    "username": "username",
    "password": "password"
  }
  ```

[aarch64-shield]: https://img.shields.io/badge/aarch64-yes-green.svg
[amd64-shield]: https://img.shields.io/badge/amd64-yes-green.svg
[armhf-shield]: https://img.shields.io/badge/armhf-yes-green.svg
[i386-shield]: https://img.shields.io/badge/i386-yes-green.svg
[anchore-shield]: https://anchore.io/service/badges/image/023a2818b2274b9bb3a7dae5eeb75a6f523c44b2827a7d624a86a2c05f72106a
[anchore]: https://anchore.io/image/dockerhub/timmo001%2Fhome-panel-api%3Alatest
[dockerhub]: https://hub.docker.com/r/timmo001/home-panel-api
[layers-shield]: https://images.microbadger.com/badges/image/timmo001/home-panel-api.svg
[microbadger]: https://microbadger.com/images/timmo001/home-panel-api
[pulls-shield]: https://img.shields.io/docker/pulls/timmo001/home-panel-api.svg
[version-shield]: https://images.microbadger.com/badges/version/timmo001/home-panel-api.svg
[buymeacoffee-shield]: https://www.buymeacoffee.com/assets/img/guidelines/download-assets-sm-2.svg
[buymeacoffee]: https://www.buymeacoffee.com/timmo
